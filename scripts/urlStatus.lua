local name = ''
local greeting = name
local client

local function main()
  -- write app code in local scope, using API
end
Script.register("Engine.OnStarted", main)
-- serve API in global scope

local function performHttpGetRequest(url)
  -- Create HTTPClient handle
  client = HTTPClient.create()
  if not client then
    print('Error creating handle')
    return
  end

  client:setCABundle('resources/cacert.pem')
  client:setPeerVerification(true)
  client:setHostnameVerification(false)

  -- Create request
  local request = HTTPClient.Request.create()
  request:setURL(url)
  -- The port is selected automatically based on the protocol (http/https) but
  -- can be set manually if needed by uncommenting this line:
  -- request:setPort(port)
  request:setMethod('GET')

  -- Execute request
  local response = client:execute(request)

  -- Check success
  local success = response:getSuccess()
  print(success)
  if not success then
    local error = response:getError()
    local errorDetails = response:getErrorDetail()
    print('Error: ' .. error)
    print('Detail: ' .. errorDetails)
  end

  if success then
    -- Print HTTP Status code
    print('Status code: ' .. response:getStatusCode())

    -- Output HTTP response headers
    for _, v in ipairs(response:getHeaderKeys()) do
      local _, values = response:getHeaderValues(v)
      local pKey = string.format('  > %s: %s', v, table.concat(values, ', '))
      --print ('track' ..v ..table.concat(values, ', '))
      local headerData = pKey
      Script.notifyEvent("getHeaderData", headerData)
    end
  

    -- Output content summary
    print(
      string.format('\nReceived %d bytes\n', string.len(response:getContent()))
    )
    local okData = response:getStatusCode()
    Script.notifyEvent("getStatusdata", okData)
    --Script.notifyEvent("getStatuspic", okData)

  local code = response:getStatusCode()
    if code == 200 then
      Script.notifyEvent("getStaCode", code)
      print('status is ok')
      -- else
      --   Script.notifyEvent("getStatusdata", okData)
      --   print('failed')
    end

    local code2 = response:getStatusCode()
    if not code2 == 200 then
      Script.notifyEvent("getStaCode2", code2)
      print('status is failed')
    end

  end
end
Script.serveEvent("HttpClient_Status.getStatusdata", "getStatusdata")
Script.serveEvent("HttpClient_Status.getStatuspic", "getStatuspic")
Script.serveEvent("HttpClient_Status.getStaCode", "getStaCode")
Script.serveEvent("HttpClient_Status.getStaCode2", "getStaCode2")
Script.serveEvent("HttpClient_Status.getHeaderData", "getHeaderData")


--@setName(change:string):
local function setName(change)
  name = change
  greeting = name
  Script.notifyEvent("greetingUpdated", greeting)
  print (performHttpGetRequest(greeting))
end
Script.serveFunction("HttpClient_Status.setName", setName)
Script.serveEvent("HttpClient_Status.greetingUpdated", "greetingUpdated")

--@getGreeting():string
local function getGreeting()
  return  greeting
end

Script.serveFunction("HttpClient_Status.getGreeting", getGreeting)
